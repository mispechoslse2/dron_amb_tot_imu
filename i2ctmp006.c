/* XDCtools Header files */
#include <xdc/std.h>
#include <xdc/runtime/System.h>

/* BIOS Header files */
#include <time.h>
#include <math.h>
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Swi.h>
#include <ti/sysbios/knl/Semaphore.h>
#include <ti/sysbios/knl/Clock.h>


/* TI-RTOS Header files */
#include <ti/drivers/GPIO.h>
#include <ti/drivers/PWM.h>
#include <ti/drivers/SPI.h>
#include <ti/drivers/ADC.h>
#include <ti/drivers/I2C.h>


/* Example/Board Header files */
#include "Board.h"



#define TASKSTACKSIZE       640
int angle;
int a = 0;
int b;
int conta;
int contat;
int estat;
int dutypos;
int dutyneg;


int dutyInc0;                                   //varable que augmenatar� la velocitat del motor 0
int dutyInc1;                                   //varable que augmenatar� la velocitat del motor 1
int dutyInc2;                                   //varable que augmenatar� la velocitat del motor 2
int dutyInc3;                                   //varable que augmenatar� la velocitat del motor 3
int dutyNC1 = 1250;                             //variables per fer l'escala de PWM
int dutyNC2 = 1250;


Task_Struct task0Struct;
Char task0Stack[TASKSTACKSIZE];
int ja = 0;
float acc_x_g;
float acc_y_g;
float acc_z_g;
float Gir_x_g;
float Gir_y_g;
float Angulo_euler_acc[2];
float Total_angle[2];
float min;
float max;


float rad_to_deg = 180/3.141592654;

float valor_PID_x;
double error_anterior_P_x; //Valor antiguo del �ngulo PITCH.
double X_I=0; //Valor integral del PID del �ngulo PITCH.
double X_D;//valor derivativo del PID del �ngulo PITCH.
double X_P;//Valor proporcional del PID del �ngulo PITCH.

double Kp_x=1.5;//3.55
double Ki_x=0.0006;//0.003
double Kd_x=2;//2.05
//double Kp_x=18; //Constante de proporcionalidad.
//double Ki_x=0; //Constante de integraci�n.
//double Kd_x=20; //Constante de derivaci�n.


double error_P_x;



float valor_PID_y;
double error_anterior_P_y; //Valor antiguo del �ngulo PITCH.
double Y_I=0; //Valor integral del PID del �ngulo PITCH.
double Y_D;//valor derivativo del PID del �ngulo PITCH.
double Y_P;//Valor proporcional del PID del �ngulo PITCH.

double Kp_y=1.5;//3.55
double Ki_y=0.0006;//0.003
double Kd_y=2;//2.05
//double Kp_y=8; //Constante de proporcionalidad.
//double Ki_y=0; //Constante de integraci�n.
//double Kd_y=20; //Constante de derivaci�n.


double error_P_y;



double sample = 10;
double timeStep;
double timePrev = 0;

/*
 *  ======== taskFxn ========
 *  Task for this function is created statically. See the project's .cfg file.
 */
int16_t        Acel_X_LH;
int16_t        Acel_Y_LH;
int16_t        Acel_Z_LH;
int16_t        Gir_x;
int16_t        Gir_y;

int conta = 1;


void gir_dreta(unsigned int arg0){
    if (a!=0){
        a = 0;
        ja = 0;
    }
    if (a==0){
        a = 1;
    }

}

void gir_esquerra(unsigned int arg0){
    if (a!=0){
        a = 0;
        ja = 0;
    }
    if (a==0){
        a = 2;
    }
}




Void leer_imu(UArg arg0, UArg arg1)
{
    PWM_Handle pwm0;                            //definim el handle del pwm del motor 0
    PWM_Handle pwm1;                            //definim el handle del pwm del motor 1
    PWM_Handle pwm2;                            //definim el handle del pwm del motor 2
    PWM_Handle pwm3;                            //definim el handle del pwm del motor 3

    PWM_Params params;                          //definim els parametres del pwm
    uint16_t   pwmPeriod = 20000;               // Period and duty in microseconds



    PWM_Params_init(&params);                   //configurem els parametres del pwm amb els valors que ens interesen
    params.dutyUnits = PWM_DUTY_US;
    params.dutyValue = 2000;
    params.periodUnits = PWM_PERIOD_US;
    params.periodValue = pwmPeriod;
    //params.dutyMode = PWM_DUTY_TIME;
    pwm0 = PWM_open(Board_PWM0, &params);
    pwm1 = PWM_open(Board_PWM1, &params);
    pwm2 = PWM_open(Board_PWM2, &params);
    pwm3 = PWM_open(Board_PWM3, &params);

    if (pwm0 == NULL) {                         //abortem en el cas que no s'obri el notstre PWM (p2.6)
        System_abort("Board_PWM0 did not open");
    }
    if (pwm1 == NULL) {                         //abortem en el cas que no s'obri el notstre PWM (p2.6)
        System_abort("Board_PWM1 did not open");
    }
    if (pwm2 == NULL) {                         //abortem en el cas que no s'obri el notstre PWM (p2.6)
        System_abort("Board_PWM2 did not open");
    }
    if (pwm3 == NULL) {                         //abortem en el cas que no s'obri el notstre PWM (p2.6)
        System_abort("Board_PWM3 did not open");
    }
    PWM_start(pwm0);                            //iniciem el PWM del motor 0
    PWM_start(pwm1);                            //iniciem el PWM del motor 1
    PWM_start(pwm2);                            //iniciem el PWM del motor 2
    PWM_start(pwm3);                            //iniciem el PWM del motor 3


    uint8_t         txBuffer[1];
    uint8_t         rxBuffer[14];
    I2C_Handle      i2c;
    I2C_Params      i2cParams;
    I2C_Transaction i2cTransaction;

    /* Create I2C for usage */
    I2C_Params_init(&i2cParams);
    i2cParams.bitRate = I2C_400kHz;
    i2c = I2C_open(Board_I2C_TMP, &i2cParams);
    if (i2c == NULL) {
        System_abort("Error Initializing I2C\n");
    }
    else {
        System_printf("I2C Initialized!\n");
    }

    PWM_setDuty(pwm0, 1000);       //introduim el valor del duty (1ms)
    PWM_setDuty(pwm1, 1000);       //introduim el valor del duty (1ms+duntyinc)
    PWM_setDuty(pwm2, 1000);       //introduim el valor del duty (1ms+duntyinc)
    PWM_setDuty(pwm3, 1000);       //introduim el valor del duty (1ms)


/* Activamos la IMU�
 *   */
        txBuffer[0] = 0x6B;
        txBuffer[1] = 0;
        i2cTransaction.slaveAddress = 0x68;
        i2cTransaction.writeBuf = txBuffer;
        i2cTransaction.writeCount = 2;


        clock_t time;


    while (1)
      {
        time = Clock_getTicks();
        timeStep =(time - timePrev);

        /* Pedimos las acc + gyr */
        txBuffer[0] = 0x3B;
        i2cTransaction.slaveAddress = 0x68;
        i2cTransaction.writeBuf = txBuffer;
        i2cTransaction.writeCount = 1;
        i2cTransaction.readBuf = rxBuffer;
        i2cTransaction.readCount = 14;
        I2C_transfer(i2c, &i2cTransaction);
    //calculem l'acceleraci� x,y i z sumant el low i high
        Acel_X_LH = (rxBuffer[0] << 8) | (rxBuffer[1]);
        acc_x_g= Acel_X_LH/16384.0; // >>16 despla�ar 16 posicions
        Acel_Y_LH = (rxBuffer[2] << 8) | (rxBuffer[3]);
        acc_y_g= Acel_Y_LH/16384.0;
        Acel_Z_LH = (rxBuffer[4]<<8  | rxBuffer[5]);
        acc_z_g= Acel_Z_LH/16384.0;
        //calculem el giroscopi x i y sumant el low i high
        Gir_x = (rxBuffer[8]<<8  | rxBuffer[9]);
        Gir_y = (rxBuffer[10]<<8  | rxBuffer[11]);
        //Calibrem el valor del giroscopi dividint pel valor que ens dona el datasheet de la imu
        //Seria el angulo de euler del giroscopio.
        Gir_x_g= Gir_x/132.0;
        Gir_y_g= Gir_y/132.0;

        //Calculamos los angulos de euler de la acceleraci�n
        //Angulo x
        Angulo_euler_acc[0] = atan((acc_y_g)/sqrt(pow((acc_x_g),2) + pow((acc_z_g),2)))*rad_to_deg;
        //Angulo y
        Angulo_euler_acc[1] = atan(-1*(acc_x_g)/sqrt(pow((acc_y_g),2) + pow((acc_z_g),2)))*rad_to_deg;


        //calibramos los primeros valores de acc
        if (conta == 1) {
            Total_angle[0]=Angulo_euler_acc[0];
            Total_angle[1]=Angulo_euler_acc[1];
          }
        //Calcular angulo de rotaci�n con giroscopio y filtro complementario
        else{
            /*---X axis angle---*/
            Total_angle[0] = 0.98 *(Total_angle[0] + Gir_x_g*0.01) + 0.02*Angulo_euler_acc[0];
            /*---Y axis angle---*/
            Total_angle[1] = 0.98 *(Total_angle[1] + Gir_y_g*0.01) + 0.02*Angulo_euler_acc[1];
          }
        conta=0;

        if(a==1 & ja==0){
            Total_angle[1] = Total_angle[1]+60;
            ja = 1;
        }
        if(a==2 & ja==0){
            Total_angle[1] = Total_angle[1]-60;
            ja = 1;
        }


// PID
//        if (estat == 1){
            if (timeStep >= sample)
                {
                timePrev = time;
                //Se calcula el error
                error_P_x = 0-Total_angle[0]; //El error es la diferencia del angulo_actual - angul_referencia.
                error_P_y = 0-Total_angle[1]; //El error es la diferencia del angulo_actual - angul_referencia.


                /////////////////////XXX//////////////////////
                X_P = Kp_x*error_P_x; //Componente de valor proporcional al �ngulo
                if(-10<error_P_x<10)
                {
                X_I = X_I + (Ki_x*error_P_x);
                }//Componente para integrar la velocidad del angulo
                X_D = (Kd_x*((error_P_x - error_anterior_P_x)));

                //Resultante PID, u = P + I + D.
                valor_PID_x = (X_P+X_I+X_D); //Valor PID del �ngulo PITCH.



                /////////////////////YYY//////////////////////
                Y_P = Kp_y*error_P_y; //Componente de valor proporcional al �ngulo
                if(-10<error_P_y<10)                //CANVIAR AIXO PER ELS SWI
                {
                Y_I = Y_I + (Ki_y*error_P_y);
                }//Componente para integrar la velocidad del angulo
                Y_D = (Kd_y*((error_P_y - error_anterior_P_y)));

                //Resultante PID, u = P + I + D.
                valor_PID_y = (Y_P+Y_I+Y_D); //Valor PID del �ngulo PITCH.


                //El PID solo va de -1000 a 1000
                if(valor_PID_x>1000){//Si el valor de PID es mayor que 1000, fijamos su valor a 1000.
                        valor_PID_x = 1000;
                    }
                if(valor_PID_x < -1000){//Si el valor de PID es menor que -1000, fijamos su valor a -1000.
                            valor_PID_x = -1000;
                        }

                //El PID solo va de -1000 a 1000
                if(valor_PID_y>1000){//Si el valor de PID es mayor que 1000, fijamos su valor a 1000.
                    valor_PID_y = 1000;
//                    if (valor_PID_y>1){
//                        valor_PID_y = 1;
//                    }
                }
                if(valor_PID_y < -1000){//Si el valor de PID es menor que -1000, fijamos su valor a -1000.
                    valor_PID_y = -1000;
//                    if (valor_PID_y<-1){
//                        valor_PID_y = -1;
//                    }
                }


                dutyInc0 = 1280-valor_PID_x;
                dutyInc2 = 1250+valor_PID_x;
                error_anterior_P_x = error_P_x;

                dutyInc1 = 1280-valor_PID_y;
                dutyInc3 = 1250+valor_PID_y;
                error_anterior_P_y = error_P_y;


                }



                //Mapeamos el PWM para valores de 1100 a 1500
                if(dutyInc0>1300){
                    dutyInc0 = 1300;
                }
                if(dutyInc0 < 1000){
                    dutyInc0 = 1000;
                }

                if(dutyInc1>1400){
                    dutyInc1 = 1400;
                }
                if(dutyInc1 < 1100){
                    dutyInc1 = 1100;
                }

                if(dutyInc2>1300){
                    dutyInc2 = 1300;
                }
                if(dutyInc2 < 1000){
                    dutyInc2 = 1000;
                }

                if(dutyInc3>1300){
                    dutyInc3 = 1300;
                }
                if(dutyInc3 < 1050){
                    dutyInc3 = 1050;
                }





        //Cambiamos el duty de las PWM con el valor calculado
            PWM_setDuty(pwm0, dutyInc0);
            PWM_setDuty(pwm1, dutyInc1);
            PWM_setDuty(pwm2, dutyInc2);
            PWM_setDuty(pwm3, dutyInc3);


            //Task_sleep((UInt) arg0);
           //Task_sleep(1000);
            }

        System_flush();



    /* Deinitialized I2C */
    //I2C_close(i2c);
    //System_printf("I2C closed!\n");


}
int main(void)
{
    min = -10.0;
    max = 10.0;
    Task_Params taskParams;

    /* Call board init functions */
    Board_initGeneral();
    Board_initGPIO();
    Board_initI2C();
    Board_initPWM();


    /* Construct tmp006 Task thread */
    Task_Params_init(&taskParams);
    taskParams.stackSize = TASKSTACKSIZE;
    taskParams.stack = &task0Stack;
    taskParams.arg0 = 50;
    Task_construct(&task0Struct, (Task_FuncPtr)leer_imu, &taskParams, NULL);

    GPIO_setCallback(Board_BUTTON0, gir_dreta);
    GPIO_enableInt(Board_BUTTON0);

    GPIO_setCallback(Board_BUTTON1, gir_esquerra);
    GPIO_enableInt(Board_BUTTON1);
    /* SysMin will only print to the console when you call flush or exit */
    System_flush();

    /* Start BIOS */
    BIOS_start();

    return (0);
}
